const root = document.querySelector('#root');

function App() {
    const [activity, setActivity] = React.useState('');
    const [todos, setTodos] = React.useState([]);
    const [edit, setEdit] = React.useState({});
    const [message, setMessage] = React.useState('');

    function generateId() {
        return Date.now();
    }

    function saveTodoHandler(event) {
        event.preventDefault();

        if (!activity) {
            return setMessage('Nama Aktivitas jangan kosong')
        }

        setMessage('')

        if (edit.id) {
            const updatedTodo = {
                ...edit,
                activity
            }
            const editTodoIndex = todos.findIndex(function (todo) {
                return todo.id === edit.id;
            });

            const updatedTodos = [...todos];
            updatedTodos[editTodoIndex] = updatedTodo;

            setTodos(updatedTodos);

            return cancelEditHandler();
        }

        setTodos([...todos, {
            id: generateId(),
            activity: activity,
            isDone: false
        }])
        setActivity('')
        setMessage('')
    }

    function removeTodoHandler(todoId) {
        const filteredTodos = todos.filter(function (todo) {
            return todo.id !== todoId
        })
        setTodos(filteredTodos)
        if (edit.id) cancelEditHandler();
    }

    function editTodoHandler(todo) {
        setActivity(todo.activity)
        setEdit(todo);
    }

    function cancelEditHandler() {
        setEdit({});
        setActivity('');
    }

    function doneTodoHandler(todo) {
        const updatedTodo = {
            ...todo,
            isDone: todo.isDone ? false : true
        }

        const editTodoIndex = todos.findIndex(function (currentTodo) {
            return currentTodo.id == todo.id
        })

        const updatedTodos = [...todos]
        updatedTodos[editTodoIndex] = updatedTodo;

        setTodos(updatedTodos);
    }

    return (
        <>
            <h1>Simple Todo List</h1>
            {message && <div style={{ color: 'red', marginBottom: '16px' }}>{message}</div>}
            <form onSubmit={saveTodoHandler}>
                <input type="text" value={activity} placeholder="Nama Aktivitas" onChange={function (event) {
                    setActivity(event.target.value)
                }} />
                <button type="submit">{edit.id ? 'Update' : 'Simpan'}</button>
                {edit.id && (<button onClick={cancelEditHandler}>Cancel</button>)}
            </form>
            {todos.length > 0 ? (<ul>
                {todos.map(function (todo) {
                    return <li key={todo.id}>< input type="checkbox" checked={todo.isDone} onChange={doneTodoHandler.bind(this, todo)} />{todo.isDone ? <s>{todo.activity}</s> : todo.activity} | <button onClick={editTodoHandler.bind(this, todo)}>Edit</button> | <button onClick={removeTodoHandler.bind(this, todo.id)}>Hapus</button></li>
                })}
            </ul>) : <p>Tidak ada Todo</p>}
        </>
    )
}

ReactDOM.render(<App />, root);